#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------


def one2dict(stmt, cursor):
    """ return a dict with (column name,value) pairs
    match the return of fetchone()
    """
    d = {}
    for col in stmt.columns:
        d[col.name] = getattr(cursor, col.name)
    return d


def row2dict(stmt, row):
    """ transform 1 row returned by DB dict """
    d = {}
    for col, val in zip(stmt.columns, row):
        d[col.name] = val
    return d


def rows2dict(stmt, rows):
    """ transform the rows returned by DB into a list of dicts
    match the return of fetchall()
    """
    return [row2dict(stmt, row) for row in rows]
