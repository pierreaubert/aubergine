#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import re
import ply.lex


class Query_Lex:

    # reserved list of words
    _reserved = {
        'AND': 'AND',
        'OR': 'OR',
        'NOT': 'NOT',
    }

    # list of tokens
    tokens = ['LPAREN', 'RPAREN', 'COLON', 'NAME', 'QUOTE'] \
             + list(_reserved.values())

    t_ignore = ' '
    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_COLON = r':'
    t_QUOTE = r'"'

    # cheat the lexer and return an ID if name match a reserved word
    def t_NAME(self, t):
        ur'\w+'
        t.type = self._reserved.get(t.value, 'NAME')
        return t

    def lexer(self):
        return ply.lex.lex(optimize=1, module=self, reflags=re.UNICODE)
        # return ply.lex.lex(module=self, reflags=re.UNICODE)

    # Error handling rule
    def t_error(self, t):
        print "Illegal character '%s'" % t.value[0]
        t.lexer.skip(1)
