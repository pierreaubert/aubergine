#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from sqlalchemy import text
from aubergine.query import QueryParser
from aubergine.table import SqlFullTextAubergine


def _options2sql(options):
    """ parse option *tokenizer* and return an associated sql """
    tokenizer = None
    lang = None
    if options is not None:
        if 'tokenizer' in options:
            tokenizer = options['tokenizer']
            if tokenizer[:3] == 'icu':
                lang = tokenizer[4:]
                tokenizer = tokenizer[:3]
            if tokenizer not in ('simple', 'unicode61', 'icu', 'porter'):
                raise Exception('tokenizer is not supported')
    sql_tokenizer = ''
    if tokenizer is not None:
        sql_tokenizer = ', tokenize={0}'.format(tokenizer)
        if lang:
            sql_tokenizer += ' {0}'.format(lang)
        if 'remove_diacritics' in options:
            remove_diacritics = options['remove_diacritics']
            if remove_diacritics:
                sql_tokenizer += ' "remove_diacritics=1"'
            else:
                sql_tokenizer += ' "remove_diacritics=0"'
    return sql_tokenizer


class Sqlite3FullTextAubergine(SqlFullTextAubergine):
    """ implementation for sqlite3 """

    def create_schema(self, tablename):
        """ nothing to do for sqlite3 """
        pass

    def add_fulltext(self, tablename, cols, options=None):
        """ create a full text index for *tablename* on columns *cols*
        and 4 triggers
        """
        sql_cols = ', '.join(cols)
        new_cols = 'NEW.'+', NEW.'.join(cols)

        # do we build a specific tokenizer from options
        sql_tokenizer = _options2sql(options)

        # sql one statement at a time
        sql1 = '''
CREATE VIRTUAL TABLE IF NOT EXISTS {table}_ft using fts4(content="{table}", {cols} {tok});
        '''.format(table=tablename,
                   cols=sql_cols,
                   tok=sql_tokenizer)
        sql2 = '''
CREATE TRIGGER IF NOT EXISTS {0}_ft_bu BEFORE UPDATE ON {0} BEGIN
       DELETE FROM {0}_ft WHERE docid = OLD.rowid;
END;
        '''.format(tablename, sql_cols, new_cols)
        sql3 = '''
CREATE TRIGGER IF NOT EXISTS {0}_ft_bd BEFORE DELETE ON {0} BEGIN
       DELETE FROM {0}_ft WHERE docid = OLD.rowid;
END;
        '''.format(tablename, sql_cols, new_cols)
        sql4 = '''
CREATE TRIGGER IF NOT EXISTS {0}_ft_au AFTER UPDATE ON {0} BEGIN
       INSERT INTO {0}_ft(docid, {1} ) VALUES (NEW.rowid, {2} );
END;
        '''.format(tablename, sql_cols, new_cols)
        sql5 = '''
CREATE TRIGGER IF NOT EXISTS {0}_ft_ai AFTER INSERT ON {0} begin
       INSERT INTO {0}_ft(docid, {1} ) VALUES (NEW.rowid, {2} );
END;
        '''.format(tablename, sql_cols, new_cols)
        #
        self.logger.debug('create full text index in sqlite3 database for table {0}({1}).'.
                          format(tablename, sql_cols))
        self.conn.execute(text(sql1))
        self.conn.execute(text(sql2))
        self.conn.execute(text(sql3))
        self.conn.execute(text(sql4))
        self.conn.execute(text(sql5))

    def search(self, tablename, query):
        ast = QueryParser().parse(query)
        sql = '''SELECT * FROM {0}_ft WHERE {0}_ft MATCH \'{1}\';'''.format(tablename, ast)
        rows = self.conn.execute(text(sql)).fetchall()
        return [row for row in rows]
