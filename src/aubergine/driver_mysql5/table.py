#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from sqlalchemy import text
from sqlalchemy.exc import InternalError
from aubergine.query import QueryParser
from aubergine.table import SqlFullTextAubergine
from aubergine.driver_mysql5.query import ast2MySQL5


class Mysql5FullTextAubergine(SqlFullTextAubergine):
    """ implementation for mysql5 """

    def create_schema(self, tablename):
        """ nothing to do for mysql5 """
        pass

    def add_fulltext(self, tablename, cols, options=None):
        """ create a full text index for *tablename* on columns *cols*
        and 4 triggers
        """
        # will be usefull later
        self.cols = ', '.join(cols)
        # do we have already a text index?
        sql = '''
SELECT COUNT(DISTINCT index_name) 
FROM information_schema.statistics 
WHERE table_schema = '{0}' 
  AND index_name LIKE 'ft_index';
        '''.format(tablename)
        # drop the text index if we have one
        have_index = self.conn.execute(text(sql)).fetchone()
        # print 'before row'
        # for row in have_index:
        #     print row
        if len(have_index) > 0 and have_index[0] > 0:
            sql1 = '''
            ALTER TABLE {0} DROP INDEX ft_index;
            '''.format(tablename)
            self.conn.execute(text(sql1))
        # drop the text index if we have one
        have_index = self.conn.execute(text(sql)).fetchone()
        # print 'have_index2: '+str(len(have_index))+' '+str(have_index[0])
        # add a text index if needed
        sql2 = '''ALTER TABLE {0} ADD FULLTEXT INDEX ft_index({1});
        '''.format(tablename, self.cols)
        self.logger.debug(
            'create full text index in mysql5 database for table {0}({1}).'
            .format(tablename, self.cols))
        try:
            self.conn.execute(text(sql2))
        except InternalError:
            pass

    def search(self, tablename, query):
        ast = QueryParser().parse(query)
        mysql_query = ast2MySQL5(ast)
        sql = '''
SELECT * FROM {table} WHERE MATCH ({cols}) AGAINST (\'{query}\' IN BOOLEAN MODE);
        '''.format(cols=self.cols,
                   table=tablename,
                   query=mysql_query)
        # print sql
        rows = self.conn.execute(text(sql)).fetchall()
        return [row for row in rows]
