#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from aubergine.query import Query_And, Query_Or, Query_Not, Query_Term


def ast2PostgreSQL9(ast):
    if isinstance(ast, Query_And):
        return '{0} & {1}'.format(
            ast2PostgreSQL9(ast.q1),
            ast2PostgreSQL9(ast.q2))
    elif isinstance(ast, Query_Or):
        return '{0} | {1}'.format(
            ast2PostgreSQL9(ast.q1),
            ast2PostgreSQL9(ast.q2))
    elif isinstance(ast, Query_Not):
        return '! {0}'.format(ast2PostgreSQL9(ast.q))
    elif isinstance(ast, Query_Term):
        return '{0}'.format(ast.word)
    else:
        return None
