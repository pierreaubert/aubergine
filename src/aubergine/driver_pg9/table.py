#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from sqlalchemy import text
from sqlalchemy.exc import ProgrammingError
from aubergine.query import QueryParser
from aubergine.table import SqlFullTextAubergine
from aubergine.driver_pg9.query import ast2PostgreSQL9


class Postgresql9FullTextAubergine(SqlFullTextAubergine):
    """ implementation for pg9 """

    def create_schema(self, tablename):
        """ nothing to do for pg9 """
        pass

    def drop_fulltext(self, tablename, cols, options=None):
        """ clean schema
        """
        sql1 = '''
ALTER TABLE {0} DROP COLUMN IF EXISTS textsearchable_index_col;
        '''
        sql2 = '''
DROP INDEX IF EXISTS {0}_textsearch_idx;
        '''.format(tablename)
        sql3 = '''
DROP TRIGGER IF EXISTS {0}_tsvectorupdate ON {0};
        '''.format(tablename)
        try:
            self.conn.execute(text(sql1))
            self.conn.execute(text(sql2))
            self.conn.execute(text(sql3))
            self.logger.debug(
                'drop full text index in postgresql9 database for table {0}({1}).'.
                format(tablename, self.cols))
        except ProgrammingError:
            self.logger.error(
                'drop full text index dailed in postgresql9 database for table {0}({1}).'.
                format(tablename, self.cols))
            pass

    def add_fulltext(self, tablename, cols, options=None):
        """ create a full text index for *tablename* on columns *cols*
        and 4 triggers
        """
        sql_cols = '''coalesce({0},\'\')'''.format(cols[0])
        for col in cols[1:]:
            sql_cols += ' || coalesce({0},\'\')'.format(col)
        # will be usefull later
        self.cols = ', '.join(cols)

        sql1 = '''
ALTER TABLE {0} ADD COLUMN textsearchable_index_col tsvector;
        '''.format(tablename)
        sql2 = '''
UPDATE {0} SET textsearchable_index_col = to_tsvector('english', {1});
        '''.format(tablename, sql_cols)
        sql3 = '''
CREATE INDEX {0}_textsearch_idx ON {0} USING gin(textsearchable_index_col);
        '''.format(tablename)
        sql4 = '''
CREATE TRIGGER {0}_tsvectorupdate BEFORE INSERT OR UPDATE
ON {0} FOR EACH ROW EXECUTE PROCEDURE
tsvector_update_trigger(textsearchable_index_col, 'pg_catalog.english', {1});
        '''.format(tablename, ', '.join(cols))

        todo = True
        try:
            self.conn.execute(text(sql1))
        except ProgrammingError:
            todo = False
            pass
        try:
            self.conn.execute(text(sql2))
        except ProgrammingError:
            todo = False
            pass
        try:
            self.conn.execute(text(sql3))
        except ProgrammingError:
            todo = False
            pass
        try:
            self.conn.execute(text(sql4))
        except ProgrammingError:
            todo = False
            pass
        if todo:
            self.logger.debug(
                'create full text index in postgresql9 database for table {0}({1}).'.
                format(tablename, sql_cols))

    def search(self, tablename, query):
        ast = QueryParser().parse(query)
        ts_query = ast2PostgreSQL9(ast)
        sql = '''SELECT {cols} FROM {table} WHERE textsearchable_index_col @@ to_tsquery(\'{query}\');'''\
            .format(cols=self.cols,
                    table=tablename,
                    query=ts_query)
        rows = self.conn.execute(text(sql)).fetchall()
        return [row for row in rows]
