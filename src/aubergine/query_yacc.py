#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from aubergine.query_lex import Query_Lex
from aubergine.query import Query_Or, Query_And, Query_Not, \
    Query_Term, Query_Sequence
import ply.yacc as lalr


class Query_Yacc:

    def __init__(self):
        self.tokens = Query_Lex.tokens
        self.lexer = Query_Lex().lexer()
        self.yacc = lalr.yacc(debug=0, write_tables=0, module=self)
        # self.yacc = lalr.yacc(module=self)

    def p_error(self, p):
        # # print 'DEBUG: ' + str(p)
        pass

    def _error(self, p, msg):
        # print 'DEBUG: ' + msg
        self.p_error(p)

    def p_expr(self, p):
        '''
        expr : pexpr AND pexpr
             | pexpr OR pexpr
             | QUOTE expr QUOTE
             | NOT pexpr
             | term
        '''
        # print 'expr='+str(len(p))
        if len(p) == 4:
            if p[1] is '"':
                p[0] = Query_Sequence(p[2])
            else:
                bin_op = p[2]
                if 'AND' == bin_op:
                    p[0] = Query_And(p[1], p[3])
                elif 'OR' == bin_op:
                    p[0] = Query_Or(p[1], p[3])
                else:
                    self._error(p, 'unkown binary operator "'+bin_op+'"')
        elif len(p) == 3:
            una_op = p[1]
            if 'NOT' == una_op:
                p[0] = Query_Not(p[2])
            else:
                self._error(p, 'unkown unary operator "'+una_op+'"')
        elif len(p) == 2:
            p[0] = p[1]
        else:
            self._error(p, 'p_expr get '+str(len(p))+' parameters')

    def p_pexpr(self, p):
        '''
        pexpr : LPAREN expr RPAREN
              | term
        '''
        # print 'pexpr='+str(len(p))
        if len(p) == 4:
            p[0] = p[2]
        elif len(p) == 2:
            p[0] = p[1]
        else:
            self._error(p, 'p_pexpr get '+str(len(p))+' parameters')

    def p_term(self, p):
        '''
        term : NAME COLON NAME COLON NAME
             | NAME COLON NAME
             | NAME
        '''
        # print 'term='+str(len(p))
        if len(p) == 6:
            p[0] = Query_Term(p[1], p[3], p[5])
        elif len(p) == 4:
            p[0] = Query_Term(p[1], p[3], 1.0)
        elif len(p) == 2:
            p[0] = Query_Term(None, p[1], 1.0)
        else:
            self._error(p, 'p_term get {0} parameters'.format(len(p)))
