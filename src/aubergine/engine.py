#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from aubergine.driver_pg9.table import Postgresql9FullTextAubergine
from aubergine.driver_mysql5.table import Mysql5FullTextAubergine
from aubergine.driver_sqlite3.table import Sqlite3FullTextAubergine


def engine2aubergine(database_url):
    """ return a proxy object from engine
    if engine is a supported one: currently mysql, pg or sqlite it 
       returns a proxy object
    if not return None
    """
    if 'postgresql' in database_url:
        return Postgresql9FullTextAubergine
    elif 'mysql' in database_url:
        return Mysql5FullTextAubergine
    elif 'sqlite' in database_url:
        return Sqlite3FullTextAubergine
    else:
        return None
