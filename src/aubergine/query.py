#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------


class QueryAubergine(object):
    """
    small proxy for a query
    """
    def __init__(self, query=None):
        self.query = query

    def build(self, query):
        self.query = query

    def __str__(self):
        raise NotImplemented()


class Query_AST(object):
    def __init__(self):
        self.level = 0
        pass

    def __repr__(self):
        return 'query()'

    def __str__(self):
        return '()'


class Query_And(Query_AST):
    """ AND between expressions
    """
    def __init__(self, q1, q2):
        self.q1 = q1
        self.q2 = q2
        self.level = max(q1.level, q2.level)+1

    def __repr__(self):
        left = '{0!r}'.format(self.q1)
        if self.q1.level > 1:
            left = '({0})'.format(left)
        right = '{0!r}'.format(self.q2)
        if self.q2.level > 1:
            right = '({0})'.format(right)
        return '{0} AND {1}'.format(left, right)

    def __str__(self):
        return self.__repr__()


class Query_Or(Query_AST):
    """ OR between expressions
    the role group
    """
    def __init__(self, q1, q2):
        self.q1 = q1
        self.q2 = q2
        self.level = max(q1.level, q2.level)+1

    def __repr__(self):
        left = '{0!r}'.format(self.q1)
        if self.q1.level > 1:
            left = '({0})'.format(left)
        right = '{0!r}'.format(self.q2)
        if self.q2.level > 1:
            right = '({0})'.format(right)
        return '{0} OR {1}'.format(left, right)

    def __str__(self):
        return self.__repr__()


class Query_Sequence(Query_AST):
    """ Words must follow each other in sequence
    """
    def __init__(self, seq):
        self.seq = seq
        self.level = max(seq.level)+1

    def __repr__(self):
        return '"{0}"'.format(' '.join(self.seq))

    def __str__(self):
        return self.__repr__()


class Query_Not(Query_AST):
    """ NOT (negate) an expression
    """
    def __init__(self, q):
        self.q = q
        self.level = q.level+1

    def __repr__(self):
        if self.level <= 2:
            return 'NOT {0!r}'.format(self.q)
        else:
            return 'NOT({0!r})'.format(self.q)

    def __str__(self):
        return self.__repr__()


class Query_Term(Query_AST):
    """ a *term* has a format like: *column:word:weigth*
    where *column* is the database column the word must belong
    if no column then lookup is on the full table
    where *word* is a utf-8 encoded word
    where *weigth* is a weigth to applay in ranking for this term
    """
    def __init__(self, col, word, weigth):
        self.col = col
        self.word = word
        self.weigth = weigth
        self.level = 1

    def __repr__(self):
        if self.col:
            fcol = '{0}:'.format(self.col)
        else:
            fcol = ''
        if self.weigth != 1:
            fweigth = ':{0}'.format(self.weigth)
        else:
            fweigth = ''
        return '{0}{1}{2}'.format(fcol, self.word, fweigth)

    def __str__(self):
        return self.__repr__()


from aubergine.query_yacc import Query_Yacc


class QueryParser(QueryAubergine):
    """ parse a string and return a QueryAubergine object """

    def __init__(self):
        """ build a lexer and parser """
        self.parser = Query_Yacc()

    def print_token(self, text):
        tok = self.parser.lexer.input(text)
        while True:
            tok = self.parser.lexer.token()
            if not tok:
                break
            print tok

    def parse(self, text):
        """ parse :) """
        return self.parser.yacc.parse(text,
                                      lexer=self.parser.lexer)
