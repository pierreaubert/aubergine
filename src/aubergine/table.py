#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------


class SqlFullTextAubergine(object):
    """
    small abstract class to have a uniform way of making full text search
    in postgresql, sqlite and mysql
    """
    def __init__(self, logger, conn):
        self.logger = logger
        self.conn = conn

    def create_schema(self, tablename):
        raise NotImplementedError()

    def add_fulltext(self, tablename, cols, options=None):
        raise NotImplementedError()

    def drop_fulltext(self, tablename, cols, options=None):
        raise NotImplementedError()

    def search(tablename, query):
        raise NotImplementedError()
