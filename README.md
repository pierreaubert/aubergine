# Aubergine: full text search abstraction in sql databases

this a extension to sqlalchemy to support full text. Currently mysql,
postgresql and sqlite are supported.

## Usage

Create a table with *sqlalchemy*:
```
        table_docs = Table(
            'docs',
            meta,
            Column('doc_id', Integer, Sequence('doc_seq'), primary_key=True),
            Column('title', Text),
            Column('document', Text),
        )

```
Create a full text object to manipulate your database:
```
        ft = aubergine.SqlFullTextAubergine(logger, engine)
```
and add an index to table *docs* on columns *title* and *document*:
```
        ft.add_fulltext('docs', ['title', 'document'])
```
Simply search 'one' in table *docs* with:
```
        r = ft.search('docs', 'one')
```
or
```
        r = ft.search('docs', 'one AND two NOT three')
```

## And now

Full documentation is in the [aubergine wiki](http://bitbucket.org/pierreaubert/aubergine/wiki).

Thanks for reading this far.
