#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import os
import unittest
import logging
from sqlalchemy.engine import create_engine
from sqlalchemy import MetaData, Table, Text, Integer, \
    Column, Sequence
from aubergine.query import QueryParser
from aubergine.driver_sqlite3.table import Sqlite3FullTextAubergine
from aubergine.driver_pg9.table import Postgresql9FullTextAubergine
from aubergine.driver_mysql5.table import Mysql5FullTextAubergine

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('/tmp/{0}.log'.format(__name__))
logger.addHandler(fh)


docs = [
    {
        'title': u'0 doc one',
        'document': u'this is somewhat longer version of doc one'
    },
    {
        'title': u'1 doc two',
        'document': u'this is a longer version of doc two, intresting or not i do not know'
    },
    {
        'title': u'2 doc three: the middle man',
        'document': u'le centre n\'est pas le milieu, la moyenne n\'est pas la médiane.'
    },
    {
        'title': u'3 doc four: the middle woman',
        'document': u'l\'homme n\'est pas la femme et réciproquement'
    },
    {
        'title': u'4 doc five: the last man',
        'document': u'flask is a nice library but tornado is more sexy'
    },
    {
        'title': u'5 été',
        'document': u'check this stupid diacritics'
    },
    {
        'title': u'6 schlissen',
        'document': u'what about utf8 encoded characters'
    },
    {
        'title': u'7 manger des pommes',
        'document': u'est une maxime trés connue'
    },
]


class FullTextTests(unittest.TestCase):
    """ all tests on full text abstraction """
    DB_URI = 'sqlite:////tmp/fulltext_test.db'
    AUBERGINE = Sqlite3FullTextAubergine
    OPTIONS = {
        # 'tokenizer': 'unicode61',
        'tokenizer': 'porter',
        'remove_diacritics': True
    }

    def setUp(self):
        """ create app """
        self.query = QueryParser
        self._meta = MetaData()
        self._engine = create_engine(self.DB_URI)
        self._connection = self._engine.connect()
        self._transaction = self._connection.begin()
        self.table_docs = Table(
            'docs',
            self._meta,
            Column('doc_id', Integer, Sequence('doc_seq'), primary_key=True),
            Column('title', Text),
            Column('document', Text),
            mysql_engine='InnoDB',
            mysql_charset='utf8',
        )
        self._meta.create_all(self._engine, checkfirst=True)
        self._nested_transaction = self._connection.begin_nested()
        self.ft = self.AUBERGINE(logger, self._engine)
        self.ft.add_fulltext('docs', ['title', 'document'], self.OPTIONS)
        # should not be usefull with transactions but when a test failed
        # we could be in an unstable state
        delete_docs = self.table_docs.delete()
        self._engine.execute(delete_docs)
        # load docs
        for doc in docs:
            ins = self.table_docs.insert().values(
                title=doc['title'],
                document=doc['document'])
            self._engine.execute(ins)

    def tearDown(self):
        """ remove db """
        fname = self.DB_URI
        pos = fname.find('://')
        engine = fname[0:pos]
        if 'sqlite' in engine:
            filename = fname[pos+3:]
            if os.access(filename, os.F_OK):
                os.unlink(filename)
                pass
        elif 'postgresql' in engine or 'mysql' in engine:
            self._nested_transaction.rollback()
            self._transaction.rollback()
            self._connection.close()
            self._engine.dispose()

    def test_smoke1(self):
        """ """
        kw = 'one'
        r = self.ft.search('docs', kw)
        # print len(r)
        # print r
        self.assertEqual(len(r), 1)
        self.assertEqual(len(r[0]), 2)
        assert kw in r[0][0]
        assert kw in r[0][1]

    def test_smoke2(self):
        """ """
        kw = 'title:one'
        r = self.ft.search('docs', kw)
        self.assertEqual(len(r), 1)
        self.assertEqual(len(r[0]), 2)
        assert 'one' in r[0][0]

    def test_smoke3(self):
        """ """
        kw = 'title:one OR document:one'
        r = self.ft.search('docs', kw)
        self.assertEqual(len(r), 1)
        self.assertEqual(len(r[0]), 2)
        assert 'one' in r[0][0]


class PostgresqlFullTextTests(FullTextTests):
    """ all tests on full text abstraction """
    DB_URI = 'postgresql+psycopg2://aubergine:courgette@localhost/aubergine'
    AUBERGINE = Postgresql9FullTextAubergine


class Mysql5FullTextTests(FullTextTests):
    """ all tests on full text abstraction """
    DB_URI = 'mysql+pymysql://root@localhost/aubergine'
    AUBERGINE = Mysql5FullTextAubergine


if __name__ == '__main__':
    unittest.main()


# []
# {}
