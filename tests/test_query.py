#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: P. Aubert pierreaubert@yahoo.fr
#
# Apache 2 License
# ----------------------------------------------------------------------

import unittest
from aubergine.query import QueryParser
from aubergine.driver_pg9.query import ast2PostgreSQL9
from aubergine.driver_mysql5.query import ast2MySQL5


class QueryParserTests(unittest.TestCase):

    def setUp(self):
        self.strs = (
            'toto',
            'col:toto',
            'col:toto',
            'col:toto:2',
            'toto AND titi',
            'toto OR titi',
            'toto OR (NOT titi)',
            '(toto OR tutu) AND (NOT titi)',
        )

    def tearDown(self):
        pass

    def test_smoke1(self):
        """ check same basic queries
        """
        qp = QueryParser()
        for str in self.strs:
            # qp.print_token(str)
            ast = qp.parse(str)
            self.assertEqual(str, ast.__str__())

    def test_postgresql9(self):
        """ check same basic queries
        """
        qp = QueryParser()
        for str in self.strs:
            ast = qp.parse(str)
            print ast2PostgreSQL9(ast)

    def test_mysql9(self):
        """ check same basic queries
        """
        qp = QueryParser()
        for str in self.strs:
            ast = qp.parse(str)
            print ast2MySQL5(ast)

    def not_test_utf8(self):
        """ check same basic queries
        """
        qp = QueryParser()
        str = 'été'
        # qp.print_token(str)
        ast = qp.parse(str)
        self.assertEqual(str, ast.__str__())

if __name__ == '__main__':
    unittest.main()
